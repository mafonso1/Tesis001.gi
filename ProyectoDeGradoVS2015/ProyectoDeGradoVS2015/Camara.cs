﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;

namespace ProyectoDeGradoVS2015
{
    public partial class Camara : Form
    {
        private bool ExistenDispositivos = false;
        private FilterInfoCollection DispositivosDeVideo;
        private VideoCaptureDevice FuenteDeVideo = null;
        public Camara()
        {
            InitializeComponent();
            BuscarDispositivos();
        }

        public void CargarDispositivos(FilterInfoCollection Dispositivos)
        {
            for (int i = 0; i < Dispositivos.Count; i++)
                comboBox1Dispositivos.Items.Add(Dispositivos[i].Name.ToString());
            comboBox1Dispositivos.Text = comboBox1Dispositivos.Items[0].ToString();
        }

        public void BuscarDispositivos()
        {
            DispositivosDeVideo = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (DispositivosDeVideo.Count == 0)
                ExistenDispositivos = false;
            else
            {
                ExistenDispositivos = true;
                CargarDispositivos(DispositivosDeVideo);
            }
        }

        public void TerminarFuenteDeVideo ()
        {
            if (! (FuenteDeVideo == null))
                if ( FuenteDeVideo.IsRunning)
                {
                    FuenteDeVideo.SignalToStop();
                    FuenteDeVideo = null;
                }
        }

        private void  video_NuevoFrame (object sender , NewFrameEventArgs eventArgs)
        {
            Bitmap imagen = ( Bitmap) eventArgs.Frame.Clone();
            EspacioCamara.Image = imagen;
       }

        private void buttonIniciar_Click(object sender, EventArgs e)
        {
            if(buttonIniciar.Text == "Iniciar")
            {
                if (ExistenDispositivos)
                {
                    FuenteDeVideo = new VideoCaptureDevice(DispositivosDeVideo[comboBox1Dispositivos.SelectedIndex].MonikerString);
                    
                    FuenteDeVideo.NewFrame += new NewFrameEventHandler(video_NuevoFrame);
                    FuenteDeVideo.Start();
                    BarraEstado.Text = "Ejecutando Dispositivo";
                    buttonIniciar.Text = "Detener";
                    comboBox1Dispositivos.Enabled = false;
                    groupBox1.Text = DispositivosDeVideo[comboBox1Dispositivos.SelectedIndex].Name.ToString();

                }
                else
                    BarraEstado.Text = "Error: no se encuenta dispositivo";
            }
            else
            {
                if ( FuenteDeVideo.IsRunning)
                {
                    TerminarFuenteDeVideo();
                    BarraEstado.Text = "Dispositivo detenido";
                    buttonIniciar.Text = "Iniciar";
                    comboBox1Dispositivos.Enabled = true;
                }
                else
                {
                    MessageBox.Show("NO CARGA LA CAM");
                }
            }
        }

        private void Camara_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (buttonIniciar.Text == "Detener")
                buttonIniciar_Click(sender, e);
        }
    }

          
}
