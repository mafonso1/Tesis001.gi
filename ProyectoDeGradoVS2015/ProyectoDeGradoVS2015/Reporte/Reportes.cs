﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoDeGradoVS2015
{
    public partial class Reportes : UserControl
    {
        public Reportes()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DBControlDeAccesoEntities2 obj = new DBControlDeAccesoEntities2();
            string fecha = Convert.ToString ( dateTimePicker1.Value.Year + "/" + dateTimePicker1.Value.Month + "/" + dateTimePicker1.Value.Day);
           
            var query = from reporte in obj.TApertura
                        select new { Nombre = reporte.Fecha, Cedula = reporte.TUsuariosIdUsuarios };


               var consulta= query.Where(elemento => elemento.Nombre == fecha);
            dataGridView1.DataSource = consulta.ToList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DBControlDeAccesoEntities2 obj = new DBControlDeAccesoEntities2();
            string fecha = Convert.ToString(dateTimePicker1.Value.Year + "/" + dateTimePicker1.Value.Month + "/" + dateTimePicker1.Value.Day);

            var query = from reporte in obj.TAfiliadoAsistencia
                        select new { Nombre = reporte.Fecha, Cedula = reporte.TUsuariosIdUsuarios };


            var consulta = query.Where(elemento => elemento.Nombre == fecha);
            dataGridView1.DataSource = consulta.ToList();
        }
    }
}
