﻿namespace ProyectoDeGradoVS2015
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAccesos = new System.Windows.Forms.Button();
            this.buttonConfiguracion = new System.Windows.Forms.Button();
            this.buttonReportes = new System.Windows.Forms.Button();
            this.buttonRegistro = new System.Windows.Forms.Button();
            this.panelFoto = new System.Windows.Forms.Panel();
            this.panelCargarControles = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelCargarControles, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.38627F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.61373F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(979, 526);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.04554F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.04099F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.04554F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.04554F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.82239F));
            this.tableLayoutPanel2.Controls.Add(this.buttonAccesos, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonConfiguracion, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonReportes, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonRegistro, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panelFoto, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(973, 101);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // buttonAccesos
            // 
            this.buttonAccesos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAccesos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAccesos.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAccesos.Location = new System.Drawing.Point(354, 3);
            this.buttonAccesos.Name = "buttonAccesos";
            this.buttonAccesos.Size = new System.Drawing.Size(111, 95);
            this.buttonAccesos.TabIndex = 3;
            this.buttonAccesos.Text = "Accesos";
            this.buttonAccesos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonAccesos.UseVisualStyleBackColor = true;
            this.buttonAccesos.Click += new System.EventHandler(this.buttonAccesos_Click);
            // 
            // buttonConfiguracion
            // 
            this.buttonConfiguracion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonConfiguracion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConfiguracion.Location = new System.Drawing.Point(237, 3);
            this.buttonConfiguracion.Name = "buttonConfiguracion";
            this.buttonConfiguracion.Size = new System.Drawing.Size(111, 95);
            this.buttonConfiguracion.TabIndex = 2;
            this.buttonConfiguracion.Text = "Configuracion";
            this.buttonConfiguracion.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonConfiguracion.UseVisualStyleBackColor = true;
            this.buttonConfiguracion.Click += new System.EventHandler(this.buttonConfiguracion_Click);
            // 
            // buttonReportes
            // 
            this.buttonReportes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonReportes.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReportes.Location = new System.Drawing.Point(120, 3);
            this.buttonReportes.Name = "buttonReportes";
            this.buttonReportes.Size = new System.Drawing.Size(111, 95);
            this.buttonReportes.TabIndex = 1;
            this.buttonReportes.Text = "Reportes";
            this.buttonReportes.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonReportes.UseVisualStyleBackColor = true;
            this.buttonReportes.Click += new System.EventHandler(this.buttonReportes_Click);
            // 
            // buttonRegistro
            // 
            this.buttonRegistro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonRegistro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonRegistro.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegistro.Location = new System.Drawing.Point(3, 3);
            this.buttonRegistro.Name = "buttonRegistro";
            this.buttonRegistro.Size = new System.Drawing.Size(111, 95);
            this.buttonRegistro.TabIndex = 0;
            this.buttonRegistro.Text = "Registro";
            this.buttonRegistro.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonRegistro.UseVisualStyleBackColor = true;
            this.buttonRegistro.Click += new System.EventHandler(this.buttonRegistro_Click);
            // 
            // panelFoto
            // 
            this.panelFoto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelFoto.BackgroundImage")));
            this.panelFoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panelFoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFoto.Location = new System.Drawing.Point(471, 3);
            this.panelFoto.Name = "panelFoto";
            this.panelFoto.Size = new System.Drawing.Size(499, 95);
            this.panelFoto.TabIndex = 4;
            // 
            // panelCargarControles
            // 
            this.panelCargarControles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCargarControles.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelCargarControles.Location = new System.Drawing.Point(0, 107);
            this.panelCargarControles.Margin = new System.Windows.Forms.Padding(0);
            this.panelCargarControles.MinimumSize = new System.Drawing.Size(979, 419);
            this.panelCargarControles.Name = "panelCargarControles";
            this.panelCargarControles.Size = new System.Drawing.Size(979, 419);
            this.panelCargarControles.TabIndex = 1;
            this.panelCargarControles.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCargarControles_Paint);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 526);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(979, 419);
            this.Name = "Principal";
            this.Text = "Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Principal_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button buttonAccesos;
        private System.Windows.Forms.Button buttonConfiguracion;
        private System.Windows.Forms.Button buttonReportes;
        private System.Windows.Forms.Button buttonRegistro;
        private System.Windows.Forms.Panel panelFoto;
        private System.Windows.Forms.Panel panelCargarControles;
    }
}