﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Video.Kinect;
using AForge.Video.VFW;
using AForge.Video.Ximea;
using BarcodeLib.BarcodeReader;
using System.IO;
using System.IO.Ports;

namespace ProyectoDeGradoVS2015
{
    public partial class Acceso : Form
    {
        int flag = 0;
        string UsuarioAcceso = "";
        string FechaAcceso = "";
        //Para llenar el datagriview
        DBControlDeAccesoEntities2 contexto = new DBControlDeAccesoEntities2();
        public Acceso()
        {
            InitializeComponent();
        }
        //Lista de Dispositivos
        private FilterInfoCollection Dispositivos;
        //Fuente de video
        private VideoCaptureDevice FuenteDeVideo;

   

     
        void EmpezarALeer()
        {
            timer1.Enabled = true;
            //establecer el dispositivo seleccionado como fuente de video
            FuenteDeVideo = new VideoCaptureDevice(Dispositivos[comboBox1.SelectedIndex].MonikerString);
            // iniciar el control
            videoSourcePlayer1.VideoSource = FuenteDeVideo;
            //iniciar recepcion de imagenes
            videoSourcePlayer1.Start();
        }

     
        void DejarDeLeer()
        {
            timer1.Enabled = false;
            //detener recepcion de imagenes
            videoSourcePlayer1.SignalToStop();
        }
        public void aperturaSP()
        {
            //configura el puerto serial
            SerialPort serialPort1 = new SerialPort();

            serialPort1.PortName = "COM3";
            serialPort1.BaudRate = 9600;
            serialPort1.DataBits = 8;
            serialPort1.Parity = Parity.None;
            serialPort1.StopBits = StopBits.One;
            serialPort1.Handshake = Handshake.None;

            serialPort1.Open();
            serialPort1.WriteLine("1");
            serialPort1.Close();

        }

        
   

     
 
        

        private void Acceso_Load(object sender, EventArgs e)
        {
            //Listar dispositivo de entrada
            Dispositivos = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            // cargar todos dispositivos al combo
            foreach (FilterInfo x in Dispositivos)
            {
                comboBox1.Items.Add(x.Name);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            EmpezarALeer();
            button1.Enabled = false;
        }

        private void Acceso_FormClosing(object sender, FormClosingEventArgs e)
        {
            DejarDeLeer();
            button1.Enabled = true;
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            try
            {
                // estar seguros que hay una imagen desde la webcam
                if (videoSourcePlayer1.GetCurrentVideoFrame() != null)
                {
                    //obtener imagen de la web cam
                    Bitmap img = new Bitmap(videoSourcePlayer1.GetCurrentVideoFrame());
                    //utilizar la libreria y leer codigo
                    string[] resultados = BarcodeReader.read(img, BarcodeReader.QRCODE);
                    // quitar la imagen de memoria
                    img.Dispose();
                  
                    if (resultados != null && resultados.Count() > 0)
                    {
                        if (resultados[0].IndexOf("1111") != -1)
                        {

                            resultados[0] = resultados[0].Replace("1111", "");
                            //agregar el texto obtenido a la lista
                            listBox1.Items.Add(resultados[0]);
                            //Para Llenar el DataGridView
                            var listaUsuarios = contexto.TUsuarios.ToList();
                            //Temporal
                            string posicion = Convert.ToString(resultados[0]);
                            foreach (var usuarios in listaUsuarios)
                            {
                                if (usuarios.Cedula == Convert.ToInt32(posicion))
                                {
                                    /*this.pictureBox1.Image = ConvertBytetoImage(usuarios.UsuarioFoto);*/
                                    if (usuarios.Estatus == 1)
                                    {
                                     
                                        flag = 1;
                                        UsuarioAcceso = Convert.ToString(usuarios.IdUsuarios);
                                        FechaAcceso = dateTimePicker1.Value.Year + "/" + dateTimePicker1.Value.Month + "/" + dateTimePicker1.Value.Day;
                                    }
                                    else
                                    {
                                       
                                        flag = 2;
                                    }

                                }


                            }
                            if (flag == 1)
                            {

                               
                                //this.pictureBox1.Image = ConvertBytetoImage(usuarios.UsuarioFoto);

                                TAfiliadoAsistencia Afacceso = new TAfiliadoAsistencia();
                                Afacceso.TUsuariosIdUsuarios = Convert.ToInt32(UsuarioAcceso);
                                Afacceso.Fecha = FechaAcceso;
                                contexto.TAfiliadoAsistencia.Add(Afacceso);
                                contexto.SaveChanges();

                                //aperturaSP();


                                MessageBox.Show("Bienvenido");
                                
                            }
                            if (flag == 2)
                            {
                                MessageBox.Show("Ingreso invalido/ SIRENA");
                            }
                            DejarDeLeer();


                            var query = from accesos in contexto.TAfiliadoAsistencia
                                        select new { Fecha = accesos.Fecha, Usuario = accesos.TUsuariosIdUsuarios };

                            dataGridView1.DataSource = query.ToList();


                            //dataGridView1.DataSource = null;
                            //dataGridView1.DataSource = listaUsuarios;
                            //MessageBox.Show("Lectura :" + resultados[0]);
                            EmpezarALeer();


                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Error al cargar la camara");
            }
        }
    }
}
