﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace ProyectoDeGradoVS2015
{
    public partial class UCAgregarClientes : UserControl
    {
        correo c = new correo();
        public UCAgregarClientes()
        {
            InitializeComponent();
            button1.Enabled = false;
            if(VariablesGlobales.EmpleadoActual == "1")
            {
                button1.Enabled = true;
            }
         
        }
        
        byte[] imgglobal;
        private void buttonGenerarQRUsuario_Click(object sender, EventArgs e)
        {
            qrCodeImgControl1.Text = 1111 + textBoxIngresarCedulaEmpleado.Text;
            MessageBox.Show("Codigo Generado");
            MemoryStream STREAM = new MemoryStream();
            qrCodeImgControl1.Image.Save(STREAM, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] pic = STREAM.ToArray();
            // enlace entre la variable global y la imagen en memoria(bytes)
            imgglobal = pic;
            //OBTENER LA IMAGEN DEL CODIGO QR GENERADO
            Image img = (Image)qrCodeImgControl1.Image.Clone();
           



            //GUARDAR LA IMAGEN A UN ARCHIVO .jpg
            SaveFileDialog sv = new SaveFileDialog();
            sv.AddExtension = true;
            sv.Filter = "Image JPG (*.jpg)|*.jpg";
            sv.ShowDialog();
            if (!string.IsNullOrEmpty(sv.FileName))
            {
                img.Save(sv.FileName);
            }
            img.Dispose();
        }

        private void buttonSeleccionarQR_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Seleccione 1 imagen";
            dlg.Filter = "JPG Files (*.jpg) |*.jpg|PNG Files(*.png)|*.png|All Files(*.*)|*.*";
            dlg.Multiselect = false;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //string picPath = dlg.FileName.ToString();
                //pictureBox1BusquedaQR.ImageLocation = picPath;
                this.pictureBox1.ImageLocation = dlg.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Picture foto = new Picture();
            foto.Show();
        }

        //private void buttonSeleccionarFoto_Click(object sender, EventArgs e)
        //{
        //    OpenFileDialog dlg = new OpenFileDialog();
        //    dlg.Title = "Seleccione 1 imagen";
        //    dlg.Filter = "JPG Files (*.jpg) |*.jpg|PNG Files(*.png)|*.png|All Files(*.*)|*.*";
        //    dlg.Multiselect = false;
        //    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        //string picPath = dlg.FileName.ToString();
        //        //pictureBox1BusquedaQR.ImageLocation = picPath;
        //        this.pictureBox2.ImageLocation = dlg.FileName;
        //    }
        //}
        //private byte[] ConvertFiltoBye(string sPath)
        //{
        //    try
        //    {
        //        byte[] data = null;
        //        FileInfo fInfo = new FileInfo(sPath);
        //        long numBytes = fInfo.Length;
        //        FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);
        //        BinaryReader br = new BinaryReader(fStream);
        //        data = br.ReadBytes((int)numBytes);
        //        return data;
        //    }
        //    catch
        //    {
        //        MessageBox.Show("Error al Cargar la imagen");
        //        return null;
        //    }

        //}
        private Image ConvertBytetoImage(byte[] photo)
        {
            Image newImage;
            using (MemoryStream ms = new MemoryStream(photo, 0, photo.Length))
            {
                ms.Write(photo, 0, photo.Length);
                newImage = Image.FromStream(ms, true);
            }
            return newImage;

        }
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {

                //pictureBox1.Image = qrCodeImgControl1.Image;
                DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();

                TUsuarios usuarios = new TUsuarios();
                usuarios.Nombre = textBoxIngresarNombreUsuario.Text;
                usuarios.Cedula = int.Parse(textBoxIngresarCedulaEmpleado.Text);
                usuarios.FechaNacimiento = dateTimePickerIngresarFechaNacimientoUsuario.Value.Year + "/" + dateTimePickerIngresarFechaNacimientoUsuario.Value.Month + "/" + dateTimePickerIngresarFechaNacimientoUsuario.Value.Day;
                usuarios.Correo = textBoxIngresarCorreoUsuario.Text;
                //pictureBox1.Image= ConvertBytetoImage(VariablesGlobales.FGlobal);
                //usuarios.UsuarioFoto = ConvertFiltoBye(this.pictureBox2.ImageLocation);
                //usuarios.UsuarioQR = ConvertFiltoBye(this.pictureBox1.ImageLocation);
                usuarios.UsuarioQR = imgglobal;
                usuarios.UsuarioFoto = VariablesGlobales.FGlobal;
                if (checkBox1.Checked == true)
                { usuarios.Estatus = 1; }
                else
                    usuarios.Estatus = 0;
                
                //if (checkBox1.Checked == true)
                //    usuarios.Estatus = 1;
                    
                //else
                //    usuarios.Estatus = 0;
               
                
                VariablesGlobales.CorreoGlobal = usuarios.Correo;
                





                db.TUsuarios.Add(usuarios);

                db.SaveChanges();
                FormularioCorreo envio = new FormularioCorreo();
                envio.Show();
                MessageBox.Show("Usuario Añadido Exitosamente");
                //c.enviarCorreo("mafonso@amagi.com.ve", "michaelamagi", "Envio Codigo QR", Convert.ToString(usuarios.Cedula), usuarios.Correo, textBoxIngresarCorreoUsuario.Text);
            }
            catch
            {
                MessageBox.Show("Algun valor esta repetido");
            }
        }
       

        private void textBoxIngresarCedulaEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && e.KeyChar != (char)8)
            {

                e.Handled = true;
                //MessageBox.Show("Ingrese un numero");


            }
        }

        private void textBoxIngresarNombreUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && e.KeyChar != (char)8)
            {

                e.Handled = true;
                //MessageBox.Show("Unicamente letras");

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                if (VariablesGlobales.EmpleadoActual == "1")
                {


                    DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();
                    int cedula = Convert.ToInt32(this.textBoxIngresarCedulaEmpleado.Text);
                    TUsuarios usuarios = db.TUsuarios.Single(emp => emp.Cedula == cedula);
                    usuarios.Nombre = textBoxIngresarNombreUsuario.Text;
                    usuarios.Correo = textBoxIngresarCorreoUsuario.Text;
                    usuarios.FechaNacimiento = dateTimePickerIngresarFechaNacimientoUsuario.Text;
                   

                    

                    db.SaveChanges();
                    MessageBox.Show("Cliente Actualizado");

                    textBoxIngresarNombreUsuario.Clear();
                    textBoxIngresarCedulaEmpleado.Clear();
                    textBoxIngresarCorreoUsuario.Clear();
                    pictureBox1.Image = null;
                    qrCodeImgControl1.Image = null;
                }
                else
                {

                    MessageBox.Show("ingrese al Sistema como Administrador");

                }
            }
            catch
            {
                MessageBox.Show("Ha ocurrido un Error");
            }


        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxIngresarCedulaEmpleado.Text == "")
                {
                    MessageBox.Show("ingrese los datos del cliente");
                }
                else
                {
                    qrCodeImgControl1.Text = 1111 + textBoxIngresarCedulaEmpleado.Text;
                    MessageBox.Show("Codigo Generado");
                    MemoryStream STREAM = new MemoryStream();
                    qrCodeImgControl1.Image.Save(STREAM, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] pic = STREAM.ToArray();
                    // enlace entre la variable global y la imagen en memoria(bytes)
                    imgglobal = pic;
                    //OBTENER LA IMAGEN DEL CODIGO QR GENERADO
                    Image img = (Image)qrCodeImgControl1.Image.Clone();

                    //GUARDAR LA IMAGEN A UN ARCHIVO .jpg
                    SaveFileDialog sv = new SaveFileDialog();
                    sv.AddExtension = true;
                    sv.Filter = "Image JPG (*.jpg)|*.jpg";
                    sv.ShowDialog();
                    if (!string.IsNullOrEmpty(sv.FileName))
                    {
                        img.Save(sv.FileName);
                    }
                    img.Dispose();
                }
            }
            catch
            {
                MessageBox.Show("Se ha presentado algun error");

            }
                
            
         




           
        }
    }
}
