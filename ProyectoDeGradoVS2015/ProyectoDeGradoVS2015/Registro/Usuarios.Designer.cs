﻿namespace ProyectoDeGradoVS2015
{
    partial class Usuarios
    {
     
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelArriba = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelBotones = new System.Windows.Forms.TableLayoutPanel();
            this.buttonEmpleados = new System.Windows.Forms.Button();
            this.buttonClientes = new System.Windows.Forms.Button();
            this.labelUsuario = new System.Windows.Forms.Label();
            this.tableLayoutPanelAbajo = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanelArriba.SuspendLayout();
            this.tableLayoutPanelBotones.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanelArriba, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanelAbajo, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.16408F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.64523F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.96896F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(578, 451);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanelArriba
            // 
            this.tableLayoutPanelArriba.ColumnCount = 2;
            this.tableLayoutPanelArriba.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelArriba.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 245F));
            this.tableLayoutPanelArriba.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelArriba.Controls.Add(this.tableLayoutPanelBotones, 1, 0);
            this.tableLayoutPanelArriba.Controls.Add(this.labelUsuario, 0, 0);
            this.tableLayoutPanelArriba.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelArriba.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelArriba.Name = "tableLayoutPanelArriba";
            this.tableLayoutPanelArriba.RowCount = 1;
            this.tableLayoutPanelArriba.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelArriba.Size = new System.Drawing.Size(572, 112);
            this.tableLayoutPanelArriba.TabIndex = 0;
            // 
            // tableLayoutPanelBotones
            // 
            this.tableLayoutPanelBotones.ColumnCount = 2;
            this.tableLayoutPanelBotones.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelBotones.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelBotones.Controls.Add(this.buttonEmpleados, 1, 0);
            this.tableLayoutPanelBotones.Controls.Add(this.buttonClientes, 0, 0);
            this.tableLayoutPanelBotones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelBotones.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanelBotones.Name = "tableLayoutPanelBotones";
            this.tableLayoutPanelBotones.RowCount = 1;
            this.tableLayoutPanelBotones.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelBotones.Size = new System.Drawing.Size(239, 106);
            this.tableLayoutPanelBotones.TabIndex = 0;
            // 
            // buttonEmpleados
            // 
            this.buttonEmpleados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonEmpleados.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEmpleados.Location = new System.Drawing.Point(122, 3);
            this.buttonEmpleados.Name = "buttonEmpleados";
            this.buttonEmpleados.Size = new System.Drawing.Size(114, 100);
            this.buttonEmpleados.TabIndex = 1;
            this.buttonEmpleados.Text = "Empleados";
            this.buttonEmpleados.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonEmpleados.UseVisualStyleBackColor = true;
            this.buttonEmpleados.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonClientes
            // 
            this.buttonClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonClientes.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClientes.Location = new System.Drawing.Point(3, 3);
            this.buttonClientes.Name = "buttonClientes";
            this.buttonClientes.Size = new System.Drawing.Size(113, 100);
            this.buttonClientes.TabIndex = 0;
            this.buttonClientes.Text = "Clientes";
            this.buttonClientes.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonClientes.UseVisualStyleBackColor = true;
            this.buttonClientes.Click += new System.EventHandler(this.buttonClientes_Click);
            // 
            // labelUsuario
            // 
            this.labelUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelUsuario.AutoSize = true;
            this.labelUsuario.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUsuario.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelUsuario.Location = new System.Drawing.Point(3, 0);
            this.labelUsuario.Name = "labelUsuario";
            this.labelUsuario.Size = new System.Drawing.Size(321, 112);
            this.labelUsuario.TabIndex = 1;
            this.labelUsuario.Text = "USUARIOS";
            this.labelUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanelAbajo
            // 
            this.tableLayoutPanelAbajo.ColumnCount = 2;
            this.tableLayoutPanelAbajo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.92308F));
            this.tableLayoutPanelAbajo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.07692F));
            this.tableLayoutPanelAbajo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelAbajo.Location = new System.Drawing.Point(3, 390);
            this.tableLayoutPanelAbajo.Name = "tableLayoutPanelAbajo";
            this.tableLayoutPanelAbajo.RowCount = 1;
            this.tableLayoutPanelAbajo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelAbajo.Size = new System.Drawing.Size(572, 58);
            this.tableLayoutPanelAbajo.TabIndex = 1;
            // 
            // Usuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Usuarios";
            this.Size = new System.Drawing.Size(578, 451);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanelArriba.ResumeLayout(false);
            this.tableLayoutPanelArriba.PerformLayout();
            this.tableLayoutPanelBotones.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelArriba;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelBotones;
        private System.Windows.Forms.Button buttonEmpleados;
        private System.Windows.Forms.Button buttonClientes;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelAbajo;
        private System.Windows.Forms.Label labelUsuario;
    }
}