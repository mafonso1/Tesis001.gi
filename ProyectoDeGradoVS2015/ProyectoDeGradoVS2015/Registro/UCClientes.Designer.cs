﻿namespace ProyectoDeGradoVS2015
{
    partial class UCClientes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelArriba = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelBotones = new System.Windows.Forms.TableLayoutPanel();
            this.buttonClientesBuscar = new System.Windows.Forms.Button();
            this.buttonClientesAgregar = new System.Windows.Forms.Button();
            this.labelUsuario = new System.Windows.Forms.Label();
            this.tableLayoutPanelAbajo = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanelArriba.SuspendLayout();
            this.tableLayoutPanelBotones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanelArriba, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanelAbajo, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.16408F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.64523F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.96896F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(578, 451);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanelArriba
            // 
            this.tableLayoutPanelArriba.ColumnCount = 2;
            this.tableLayoutPanelArriba.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelArriba.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 245F));
            this.tableLayoutPanelArriba.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelArriba.Controls.Add(this.tableLayoutPanelBotones, 1, 0);
            this.tableLayoutPanelArriba.Controls.Add(this.labelUsuario, 0, 0);
            this.tableLayoutPanelArriba.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelArriba.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelArriba.Name = "tableLayoutPanelArriba";
            this.tableLayoutPanelArriba.RowCount = 1;
            this.tableLayoutPanelArriba.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelArriba.Size = new System.Drawing.Size(572, 112);
            this.tableLayoutPanelArriba.TabIndex = 0;
            // 
            // tableLayoutPanelBotones
            // 
            this.tableLayoutPanelBotones.ColumnCount = 2;
            this.tableLayoutPanelBotones.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelBotones.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelBotones.Controls.Add(this.buttonClientesBuscar, 1, 0);
            this.tableLayoutPanelBotones.Controls.Add(this.buttonClientesAgregar, 0, 0);
            this.tableLayoutPanelBotones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelBotones.Location = new System.Drawing.Point(330, 3);
            this.tableLayoutPanelBotones.Name = "tableLayoutPanelBotones";
            this.tableLayoutPanelBotones.RowCount = 1;
            this.tableLayoutPanelBotones.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelBotones.Size = new System.Drawing.Size(239, 106);
            this.tableLayoutPanelBotones.TabIndex = 0;
            // 
            // buttonClientesBuscar
            // 
            this.buttonClientesBuscar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonClientesBuscar.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClientesBuscar.Location = new System.Drawing.Point(122, 3);
            this.buttonClientesBuscar.Name = "buttonClientesBuscar";
            this.buttonClientesBuscar.Size = new System.Drawing.Size(114, 100);
            this.buttonClientesBuscar.TabIndex = 1;
            this.buttonClientesBuscar.Text = "Buscar";
            this.buttonClientesBuscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonClientesBuscar.UseVisualStyleBackColor = true;
            this.buttonClientesBuscar.Click += new System.EventHandler(this.buttonClientesBuscar_Click);
            // 
            // buttonClientesAgregar
            // 
            this.buttonClientesAgregar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonClientesAgregar.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClientesAgregar.Location = new System.Drawing.Point(3, 3);
            this.buttonClientesAgregar.Name = "buttonClientesAgregar";
            this.buttonClientesAgregar.Size = new System.Drawing.Size(113, 100);
            this.buttonClientesAgregar.TabIndex = 0;
            this.buttonClientesAgregar.Text = "Agregar";
            this.buttonClientesAgregar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonClientesAgregar.UseVisualStyleBackColor = true;
            this.buttonClientesAgregar.Click += new System.EventHandler(this.buttonClientesAgregar_Click);
            // 
            // labelUsuario
            // 
            this.labelUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelUsuario.AutoSize = true;
            this.labelUsuario.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUsuario.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelUsuario.Location = new System.Drawing.Point(3, 0);
            this.labelUsuario.Name = "labelUsuario";
            this.labelUsuario.Size = new System.Drawing.Size(321, 112);
            this.labelUsuario.TabIndex = 1;
            this.labelUsuario.Text = "CLIENTES";
            this.labelUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelUsuario.Click += new System.EventHandler(this.labelUsuario_Click);
            // 
            // tableLayoutPanelAbajo
            // 
            this.tableLayoutPanelAbajo.ColumnCount = 2;
            this.tableLayoutPanelAbajo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.92308F));
            this.tableLayoutPanelAbajo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.07692F));
            this.tableLayoutPanelAbajo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelAbajo.Location = new System.Drawing.Point(3, 390);
            this.tableLayoutPanelAbajo.Name = "tableLayoutPanelAbajo";
            this.tableLayoutPanelAbajo.RowCount = 1;
            this.tableLayoutPanelAbajo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelAbajo.Size = new System.Drawing.Size(572, 58);
            this.tableLayoutPanelAbajo.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.Location = new System.Drawing.Point(64, 175);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(449, 155);
            this.dataGridView1.TabIndex = 2;
            // 
            // UCClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UCClientes";
            this.Size = new System.Drawing.Size(578, 451);
            this.Load += new System.EventHandler(this.UCClientes_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanelArriba.ResumeLayout(false);
            this.tableLayoutPanelArriba.PerformLayout();
            this.tableLayoutPanelBotones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelArriba;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelBotones;
        private System.Windows.Forms.Button buttonClientesBuscar;
        private System.Windows.Forms.Button buttonClientesAgregar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelAbajo;
        private System.Windows.Forms.Label labelUsuario;
        private System.Windows.Forms.DataGridView dataGridView1;
       

    }
}
