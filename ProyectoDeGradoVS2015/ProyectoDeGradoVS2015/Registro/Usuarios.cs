﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProyectoDeGradoVS2015
{
    public partial class Usuarios : UserControl
    {
        public Usuarios()
        {
            InitializeComponent();
        }
     
        //Metodo para Convertir que esta en base de datos
        private Image ConvertBytetoImage(byte[] photo)
        {
            Image newImage;
            using (MemoryStream ms = new MemoryStream(photo, 0, photo.Length))
            {
                ms.Write(photo, 0, photo.Length);
                newImage = Image.FromStream(ms, true);
            }
            return newImage;

        }
        private void button4Refrescar_Click(object sender, EventArgs e)
        {
       
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Empleados obj = new Empleados();
            this.Controls.Clear();
            this.Controls.Add(obj);


        }

     

        private void buttonClientes_Click(object sender, EventArgs e)
        {
            UCClientes obj = new UCClientes();
         
            this.Controls.Clear();
            this.Controls.Add(obj);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Empleados obj = new Empleados();
            this.Controls.Clear();
            this.Controls.Add(obj);
        }
    }
}
