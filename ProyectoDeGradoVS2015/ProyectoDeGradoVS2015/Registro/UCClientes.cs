﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoDeGradoVS2015
{
    public partial class UCClientes : UserControl
    {
        public UCClientes()
        {
            InitializeComponent();
        }

        private void buscarToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
           
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void buttonClientesAgregar_Click(object sender, EventArgs e)
        {
            UCAgregarClientes obj = new UCAgregarClientes();

            this.Controls.Clear();
            this.Controls.Add(obj);
        }

        private void buttonClientesBuscar_Click(object sender, EventArgs e)
        {
            BuscarClientes obj = new BuscarClientes();
            this.Controls.Clear();
            this.Controls.Add(obj);

        }

        private void UCClientes_Load(object sender, EventArgs e)
        {
            DBControlDeAccesoEntities2 obj = new DBControlDeAccesoEntities2();
            var query = from usuario in obj.TUsuarios
                        select new { Nombre=usuario.Nombre, Cedula = usuario.Cedula, Fecha = usuario.FechaNacimiento,usuario.Correo};
           
            dataGridView1.DataSource = query.ToList();
        }

        private void labelUsuario_Click(object sender, EventArgs e)
        {

        }
    }
}
