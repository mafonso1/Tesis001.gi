﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoDeGradoVS2015
{
    public partial class Empleados : UserControl
    {
        public Empleados()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AgregarEmpleados obj = new AgregarEmpleados();
            this.Controls.Clear();
            this.Controls.Add(obj);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3BuscarEmpleado_Click(object sender, EventArgs e)
        {
            BuscarEmpleados obj = new BuscarEmpleados();
            this.Controls.Clear();
            this.Controls.Add(obj);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //try
            //{

            //    DBControlDeAccesoEntities2 contexto = new DBControlDeAccesoEntities2();
            //    int cedula = Convert.ToInt32(this.textBox1.Text);
            //    TEmpleados empleados = contexto.TEmpleados.Single(emp => emp.Cedula == cedula);
            //    var listaEmpleados = contexto.TEmpleados.ToList();
            //    foreach (var empleado in listaEmpleados)
            //    {
            //        dataGridView1.DataSource = null;
            //        dataGridView1.DataSource = listaEmpleados;
            //    }
            //}
            //catch
            //{
            //    MessageBox.Show("Cedula Invalida o inexistente");
            //}
        }

        private void Empleados_Load(object sender, EventArgs e)
        {
            try
            {

               
               
                if (VariablesGlobales.EmpleadoActual == Convert.ToString( 1))
                {
                    

                    DBControlDeAccesoEntities2 obj = new DBControlDeAccesoEntities2();
                    var query = from empleado in obj.TEmpleados
                                select new { Nombre = empleado.Nombre, Cedula = empleado.Cedula };
                    dataGridViewEmpleados.DataSource = query.ToList();
                }
            }
            catch
            {
                MessageBox.Show("Error al Cargar el grid");
            }
           
               

        }

        private void buttonClientesAgregar_Click(object sender, EventArgs e)
        {
            AgregarEmpleados obj = new AgregarEmpleados();

            this.Controls.Clear();
            this.Controls.Add(obj);
        }

        private void buttonClientesBuscar_Click(object sender, EventArgs e)
        {
            BuscarEmpleados obj = new BuscarEmpleados();
            this.Controls.Clear();
            this.Controls.Add(obj);
        }
    }
}
