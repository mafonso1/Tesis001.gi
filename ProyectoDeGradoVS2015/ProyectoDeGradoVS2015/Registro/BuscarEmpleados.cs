﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProyectoDeGradoVS2015
{
    public partial class BuscarEmpleados : UserControl
    {
        public BuscarEmpleados()
        {
            InitializeComponent();
            if (VariablesGlobales.EmpleadoActual != Convert.ToString(1))
                button4Refrescar.Enabled = false;

        }

        private void buttonBuscarClientes_Click(object sender, EventArgs e)
        {
            try
            {
                DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();
                int cedula = Convert.ToInt32(this.textBox1BuscarUsuario.Text);
                TEmpleados empleados = db.TEmpleados.Single(emp => emp.Cedula == cedula);
                this.textBoxIngresarNombreUsuario.Text = empleados.Nombre;
                this.textBoxIngresarCedulaEmpleado.Text = Convert.ToInt32(empleados.Cedula).ToString();

                this.textBoxEmpleadoUser.Text = empleados.EmpleadoUser;
                this.textBoxEmpleadoPass.Text = empleados.EmpleadoPass;
                //this.qrCodeImgControl1.Image = ConvertBytetoImage(empleados.QREmpleado);
            }
            catch
            {
                MessageBox.Show("Ingrese los datos");
            }

        }
        private Image ConvertBytetoImage(byte[] photo)
        {
            Image newImage;
            using (MemoryStream ms = new MemoryStream(photo, 0, photo.Length))
            {
                ms.Write(photo, 0, photo.Length);
                newImage = Image.FromStream(ms, true);
            }
            return newImage;

        }

        private void button4Refrescar_Click(object sender, EventArgs e)
        {
        
                try
                {
                    if (VariablesGlobales.EmpleadoActual == Convert.ToString(1))
                    {
                        DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();
                        int cedula = Convert.ToInt32(this.textBox1BuscarUsuario.Text);
                        TEmpleados empleados = db.TEmpleados.Single(emp => emp.Cedula == cedula);
                        empleados.Nombre = textBoxIngresarNombreUsuario.Text;
                        empleados.EmpleadoUser = textBoxEmpleadoUser.Text;
                        empleados.EmpleadoPass = textBoxEmpleadoPass.Text;

                        db.SaveChanges();
                        MessageBox.Show("Empleado Actualizado");

                        textBoxEmpleadoPass.Clear();
                        textBoxEmpleadoUser.Clear();
                        textBoxIngresarCedulaEmpleado.Clear();
                        textBoxIngresarNombreUsuario.Clear();
                        pictureBox1.Image = null;
                        qrCodeImgControl1.Image = null;
                    }

                    else
                    {
                        MessageBox.Show("Ingrese como administrador");
                    }
                
            }
            catch
            {
                MessageBox.Show("Erroe al Actualizar el usuario");
            }
        }
    }
}
 
           
            
           

            

