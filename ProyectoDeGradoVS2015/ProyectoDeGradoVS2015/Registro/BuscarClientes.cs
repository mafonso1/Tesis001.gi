﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProyectoDeGradoVS2015
{
    public partial class BuscarClientes : UserControl
    {
        public BuscarClientes()
        {
            InitializeComponent();
        }

        private void buttonBuscarClientes_Click(object sender, EventArgs e)
        {
            try
            {
                DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();
                int cedula = Convert.ToInt32(this.textBox1BuscarUsuario.Text);
                TUsuarios usuarios = db.TUsuarios.Single(emp => emp.Cedula == cedula);
                this.textBoxIngresarNombreUsuario.Text = usuarios.Nombre;
                this.textBoxIngresarCedulaEmpleado.Text = Convert.ToInt32(usuarios.Cedula).ToString();
                this.dateTimePickerIngresarFechaNacimientoUsuario.Text = usuarios.FechaNacimiento;
                this.textBoxIngresarCorreoUsuario.Text = usuarios.Correo;
                this.qrCodeImgControl1.Image = ConvertBytetoImage(usuarios.UsuarioQR);
                try
                {
                    this.pictureBox1.Image = ConvertBytetoImage(usuarios.UsuarioFoto);
                }
                catch
                {
                    MessageBox.Show("El cliente no posee Foto en el sistema");
                }


            }

            catch
            {
                MessageBox.Show("Usuario no encontrado");
            }
        }
        private byte[] ConvertFiltoBye(string sPath)
        {
            byte[] data = null;
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fStream);
            data = br.ReadBytes((int)numBytes);
            return data;
        }
        private Image ConvertBytetoImage(byte[] photo)
        {
            Image newImage;
            using (MemoryStream ms = new MemoryStream(photo, 0, photo.Length))
            {
                ms.Write(photo, 0, photo.Length);
                newImage = Image.FromStream(ms, true);
            }
            return newImage;

        }

        private void button4Refrescar_Click(object sender, EventArgs e)
        {

            try
            {

                if (VariablesGlobales.EmpleadoActual == "1")
                {


                    DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();
                    int cedula = Convert.ToInt32(this.textBox1BuscarUsuario.Text);
                    TUsuarios usuarios = db.TUsuarios.Single(emp => emp.Cedula == cedula);
                    usuarios.Nombre = textBoxIngresarNombreUsuario.Text;
                    usuarios.Correo = textBoxIngresarCorreoUsuario.Text;
                    usuarios.FechaNacimiento = dateTimePickerIngresarFechaNacimientoUsuario.Text;

                    db.SaveChanges();
                    MessageBox.Show("Cliente Actualizado");

                    textBoxIngresarNombreUsuario.Clear();
                    textBoxIngresarCedulaEmpleado.Clear();
                    textBoxIngresarCorreoUsuario.Clear();
                    pictureBox1.Image = null;
                    qrCodeImgControl1.Image = null;
                }
                else
                {

                    MessageBox.Show("ingrese al Sistema como Administrador");

                }
            }
            catch
            {
                MessageBox.Show("Ha ocurrido un Error");
            }
        }
    }
}
