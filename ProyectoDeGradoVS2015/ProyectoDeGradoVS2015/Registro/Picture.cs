﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Video.Kinect;
using AForge.Video.VFW;
using AForge.Video.Ximea;
using BarcodeLib.BarcodeReader;
using System.IO;
using System.IO.Ports;


namespace ProyectoDeGradoVS2015
{
    public partial class Picture : Form
    {
        int flag = 0;
        //Para llenar el datagriview
        DBControlDeAccesoEntities2 contexto = new DBControlDeAccesoEntities2();
        public Picture()
        {
            InitializeComponent();
        }
        //Lista de Dispositivos
        private FilterInfoCollection Dispositivos;
        //Fuente de video
        private VideoCaptureDevice FuenteDeVideo;

        private void Picture_Load(object sender, EventArgs e)
        {
            //Listar dispositivo de entrada
            Dispositivos = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            // cargar todos dispositivos al combo
            foreach (FilterInfo x in Dispositivos)
            {
                comboBox1.Items.Add(x.Name);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EmpezarALeer();
            button1.Enabled = false;
        }
        void EmpezarALeer()
        {
            timer1.Enabled = true;
            //establecer el dispositivo seleccionado como fuente de video
            FuenteDeVideo = new VideoCaptureDevice(Dispositivos[comboBox1.SelectedIndex].MonikerString);
            // iniciar el control
            videoSourcePlayer1.VideoSource = FuenteDeVideo;
            //iniciar recepcion de imagenes
            videoSourcePlayer1.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DejarDeLeer();
            button1.Enabled = true;
        }
        void DejarDeLeer()
        {
            timer1.Enabled = false;
            //detener recepcion de imagenes
            videoSourcePlayer1.SignalToStop();
        }
        public void aperturaSP()
        {
            //configura el puerto serial
            SerialPort serialPort1 = new SerialPort();

            serialPort1.PortName = "COM3";
            serialPort1.BaudRate = 9600;
            serialPort1.DataBits = 8;
            serialPort1.Parity = Parity.None;
            serialPort1.StopBits = StopBits.One;
            serialPort1.Handshake = Handshake.None;

            serialPort1.Open();
            serialPort1.WriteLine("1");
            serialPort1.Close();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                // estar seguros que hay una imagen desde la webcam
                if (videoSourcePlayer1.GetCurrentVideoFrame() != null)
                {
                    //obtener imagen de la web cam
                    Bitmap img = new Bitmap(videoSourcePlayer1.GetCurrentVideoFrame());
                    //utilizar la libreria y leer codigo
                    string[] resultados = BarcodeReader.read(img, BarcodeReader.QRCODE);
                    // quitar la imagen de memoria
                    img.Dispose();
                    //obtener las lecturas cuando se lea algo
                    //if (resultados != null && resultados.Count() > 0)
                    //{
                    //    if (resultados[0].IndexOf("1111") != -1)
                    //    {

                    //        resultados[0] = resultados[0].Replace("1111", "");
                    //        //agregar el texto obtenido a la lista
                    //        listBox1.Items.Add(resultados[0]);
                    //        //Para Llenar el DataGridView
                    //        var listaEmpleados = contexto.TEmpleados.ToList();
                    //        foreach (var empleados in listaEmpleados)
                    //        {
                    //            dataGridView1.DataSource = null;
                    //            dataGridView1.DataSource = listaEmpleados;
                    //        }
                    //        DejarDeLeer();
                    //        MessageBox.Show("Lectura :" + resultados[0]);
                    //        EmpezarALeer();

                    //    }
                    //}
                    if (resultados != null && resultados.Count() > 0)
                    {
                        if (resultados[0].IndexOf("1111") != -1)
                        {

                            resultados[0] = resultados[0].Replace("1111", "");
                            //agregar el texto obtenido a la lista
                            listBox1.Items.Add(resultados[0]);
                            //Para Llenar el DataGridView
                            var listaUsuarios = contexto.TUsuarios.ToList();
                            //Temporal
                            string posicion = Convert.ToString(resultados[0]);
                            foreach (var usuarios in listaUsuarios)
                            {
                                if (usuarios.Cedula == Convert.ToInt32(posicion))
                                {
                                    /*this.pictureBox1.Image = ConvertBytetoImage(usuarios.UsuarioFoto);*/
                                    if (usuarios.Estatus == 1)
                                    {
                                        /*aperturaSP();
                                        MessageBox.Show("ALARMA")*/
                                        flag = 1;
                                    }
                                    else
                                    {
                                        /* this.pictureBox1.Image = ConvertBytetoImage(usuarios.UsuarioFoto);
                                         MessageBox.Show("Ingreso invalido/ SIRENA");*/
                                        flag = 2;
                                    }

                                }
                             

                            }
                            if (flag == 1)
                            {

                                aperturaSP();
                                //this.pictureBox1.Image = ConvertBytetoImage(usuarios.UsuarioFoto);
                                MessageBox.Show("Bienvenido");
                                //dataGridView1.DataSource = null;
                                //dataGridView1.DataSource = listaUsuarios;
                            }
                            if (flag == 2)
                            {
                                MessageBox.Show("Ingreso invalido/ SIRENA");
                            }
                            DejarDeLeer();
                            MessageBox.Show("Lectura :" + resultados[0]);
                            EmpezarALeer();


                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Error al cargar la camara");
            }
        }
        private Image ConvertBytetoImage(byte[] photo)
        {
            Image newImage;
            using (MemoryStream ms = new MemoryStream(photo, 0, photo.Length))
            {
                ms.Write(photo, 0, photo.Length);
                newImage = Image.FromStream(ms, true);
            }
            return newImage;

        }
        byte[] fotoglobal;

        private void buttonTomarFoto_Click(object sender, EventArgs e)
        {
            Bitmap img = new Bitmap(videoSourcePlayer1.GetCurrentVideoFrame());
            MessageBox.Show("Foto Tomada");
            MemoryStream STREAM = new MemoryStream();
            videoSourcePlayer1.GetCurrentVideoFrame().Save(STREAM, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] pic = STREAM.ToArray();
            fotoglobal = pic;
            VariablesGlobales.FGlobal = pic;
            //OBTENER LA IMAGEN 
            Image foto = (Image)videoSourcePlayer1.GetCurrentVideoFrame().Clone();

            //GUARDAR LA IMAGEN A UN ARCHIVO .jpg
            ////SaveFileDialog sv = new SaveFileDialog();
            ////sv.AddExtension = true;
            ////sv.Filter = "Image JPG (*.jpg)|*.jpg";
            ////sv.ShowDialog();
            ////if (!string.IsNullOrEmpty(sv.FileName))
            ////{
            ////    foto.Save(sv.FileName);
            ////}
            ////img.Dispose();
        }

        private void Picture_FormClosing(object sender, FormClosingEventArgs e)
        {
            DejarDeLeer();
            button1.Enabled = true;
        }
    }
}
