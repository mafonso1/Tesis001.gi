﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProyectoDeGradoVS2015
{
    public partial class AgregarEmpleados : UserControl
    {
        public AgregarEmpleados()
        {
            InitializeComponent();
            if (VariablesGlobales.EmpleadoActual != Convert.ToString( 1))
            button1.Enabled = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (VariablesGlobales.EmpleadoActual == Convert.ToString(1))
                {
                    DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();

                    TEmpleados empleado = new TEmpleados();
                    empleado.Nombre = textBoxNombreEmpleado.Text;
                    empleado.Cedula = int.Parse(textBoxCedulaEmpleado.Text);
                    empleado.EmpleadoPass = textBoxPassEmpleado.Text;
                    empleado.EmpleadoUser = textBoxUserEmpleado.Text;
                    empleado.QREmpleado = imgglobal;
                    if (checkBox1.Checked == true)
                        empleado.Estatus = 1;
                    else
                        empleado.Estatus = 0;
                    db.TEmpleados.Add(empleado);

                    db.SaveChanges();
                    MessageBox.Show("Empleado Añadido Exitosamente");
                    textBoxNombreEmpleado.Clear();
                    textBoxCedulaEmpleado.Clear();
                    textBoxUserEmpleado.Clear();
                    textBoxPassEmpleado.Clear();
                }
                else
                {
                    MessageBox.Show("Solo un Administrador puede agregar otros empleados");
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }
        private byte[] ConvertFiltoBye(string sPath)
        {
            byte[] data = null;
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fStream);
            data = br.ReadBytes((int)numBytes);
            return data;
        }
        byte[] imgglobal;
        private void button2_Click(object sender, EventArgs e)
        {
            qrCodeImgControl1.Text = 1111 + textBoxNombreEmpleado.Text + int.Parse(textBoxCedulaEmpleado.Text)+ textBoxUserEmpleado.Text + textBoxPassEmpleado.Text;
            MessageBox.Show("Codigo Generado");
            MemoryStream STREAM = new MemoryStream();
            qrCodeImgControl1.Image.Save(STREAM, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] pic = STREAM.ToArray();
            imgglobal = pic;
            //OBTENER LA IMAGEN DEL CODIGO QR GENERADO
            Image img = (Image)qrCodeImgControl1.Image.Clone();

            //GUARDAR LA IMAGEN A UN ARCHIVO .jpg
            SaveFileDialog sv = new SaveFileDialog();
            sv.AddExtension = true;
            sv.Filter = "Image JPG (*.jpg)|*.jpg";
            sv.ShowDialog();
            if (!string.IsNullOrEmpty(sv.FileName))
            {
                img.Save(sv.FileName);
            }
            img.Dispose();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Seleccione 1 imagen";
            dlg.Filter = "JPG Files (*.jpg) |*.jpg|PNG Files(*.png)|*.png|All Files(*.*)|*.*";
            dlg.Multiselect = false;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //string picPath = dlg.FileName.ToString();
                //pictureBox1BusquedaQR.ImageLocation = picPath;
                this.pictureBox1.ImageLocation = dlg.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();
            int cedula = Convert.ToInt32(this.textBoxCedulaEmpleado.Text);
            TEmpleados empleados = db.TEmpleados.Single(emp => emp.Cedula == cedula);
            empleados.Nombre = textBoxNombreEmpleado.Text;
            empleados.EmpleadoUser = textBoxPassEmpleado.Text;
            empleados.EmpleadoPass = textBoxPassEmpleado.Text;
            if (checkBox1.Checked == true)
                empleados.Estatus = 1;
            else
                empleados.Estatus = 0;




            db.SaveChanges();
            MessageBox.Show("Cliente Actualizado");

            textBoxNombreEmpleado.Clear();
            textBoxCedulaEmpleado.Clear();
            textBoxPassEmpleado.Clear();
            textBoxUserEmpleado.Clear();
           
            pictureBox1.Image = null;
            qrCodeImgControl1.Image = null;
        }
    }
}
