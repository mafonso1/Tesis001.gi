﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;

namespace ProyectoDeGradoVS2015
{
    public partial class FormularioAccesoManual : Form
    {
        public FormularioAccesoManual()
        {
            InitializeComponent();
        }
        public void aperturaSP()
        {
            //configura el puerto serial
            SerialPort serialPort1 = new SerialPort();

            serialPort1.PortName = "COM3";
            serialPort1.BaudRate = 9600;
            serialPort1.DataBits = 8;
            serialPort1.Parity = Parity.None;
            serialPort1.StopBits = StopBits.One;
            serialPort1.Handshake = Handshake.None;

            serialPort1.Open();
            serialPort1.WriteLine("1");
            serialPort1.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();
                TApertura apertura = new TApertura();
                apertura.Fecha = dateTimePicker1.Value.Year + "/" + dateTimePicker1.Value.Month + "/" + dateTimePicker1.Value.Day;
                apertura.TEmpleadosIdEmpleados = VariablesGlobales.IdEmpleadoApertura;
                apertura.TUsuariosIdUsuarios = VariablesGlobales.IdUsuarioManualGlobal;
                apertura.Motivo = textBox1.Text;
                
    
                

                db.TApertura.Add(apertura);

                db.SaveChanges();

           
                //aperturaSP();


                Principal pri = new Principal();
                pri.Show();
                this.Hide();
            }
            catch
            {
                MessageBox.Show("Érror");
            }

         
        }
    }
}
