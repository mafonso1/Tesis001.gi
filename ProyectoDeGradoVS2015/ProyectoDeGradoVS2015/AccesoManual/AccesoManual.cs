﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoDeGradoVS2015
{
    
    public partial class AccesoManual : Form
    {
        //int IdUsuarioManual;
        public AccesoManual()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DBControlDeAccesoEntities2 db = new DBControlDeAccesoEntities2();
                int cedula = Convert.ToInt32(this.textBox1.Text);

                TUsuarios usuarios = db.TUsuarios.Single(emp => emp.Cedula == cedula);
                if (usuarios.Estatus == 1)
                {
                    LoginAccesoManual login = new LoginAccesoManual();
                    //IdUsuarioManual = usuarios.IdUsuarios;
                    VariablesGlobales.IdUsuarioManualGlobal = usuarios.IdUsuarios;
                    MessageBox.Show("Usuario Activo, se procede a validar su acceso");
                    login.Show();
                    this.Hide();
                    
                }
                else
                    MessageBox.Show("Usted no esta activo, ponganse en contacto con el administrador");
            }
            catch
            {
                MessageBox.Show("Usuario No Valido");
            }
            
            
        }
    }
}
