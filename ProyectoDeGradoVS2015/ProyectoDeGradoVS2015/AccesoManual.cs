﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoDeGradoVS2015
{
    public partial class AccesoManual : Form
    {
        public AccesoManual()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DBControlDeAccesoEntities1 db = new DBControlDeAccesoEntities1();
            
            int cedula = Convert.ToInt32(this.textBox1.Text);
            TUsuarios usuarios = db.TUsuarios.Single(emp => emp.Cedula == cedula);
            if (usuarios.Estatus == 1)
            {
                Login bh = new Login();
                bh.Show();
            }
            else
                MessageBox.Show("Usuario No valido");
        }
    }
}
