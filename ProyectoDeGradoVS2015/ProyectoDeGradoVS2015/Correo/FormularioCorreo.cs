﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoDeGradoVS2015
{
    public partial class FormularioCorreo : Form
    {
        correo c = new correo();
        public FormularioCorreo()
        {
            InitializeComponent();
        }

        private void FormularioCorreo_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.openFileDialog1.ShowDialog();
                if (this.openFileDialog1.FileName.Equals("") == false)
                {
                    txtRutaArchivo.Text = this.openFileDialog1.FileName;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cargar la ruta del archivo: " + ex.ToString());
            }
        }

        private void BtnEnviar_Click(object sender, EventArgs e)
        {
             
            c.enviarCorreo(VariablesGlobales.CorreoEnvio, VariablesGlobales.CorreoContraseña, rtbMensaje.Text, txtAsunto.Text, VariablesGlobales.CorreoGlobal, txtRutaArchivo.Text);
        }

        private void txtEmisor_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
