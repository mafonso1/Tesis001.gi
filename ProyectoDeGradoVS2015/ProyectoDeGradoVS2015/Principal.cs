﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoDeGradoVS2015
{
    public partial class Principal : Form
    {
        
        public Principal()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelCargarControles_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonRegistro_Click(object sender, EventArgs e)
        {
            Usuarios controlUsuarios = new Usuarios();
               this.panelCargarControles.Controls.Clear();
            this.panelCargarControles.Controls.Add(controlUsuarios);
        }

        private void buttonConfiguracion_Click(object sender, EventArgs e)
        {

            
            //FormularioCorreo envio = new FormularioCorreo();
            //envio.Show();
            AccesoManual manual = new AccesoManual();
            manual.Show();

        }

        private void buttonAccesos_Click(object sender, EventArgs e)
        {
            Acceso pic = new Acceso();
            pic.Show();
        }

        private void buttonReportes_Click(object sender, EventArgs e)
        {
            Reportes rep = new Reportes();
            this.panelCargarControles.Controls.Clear();
            this.panelCargarControles.Controls.Add(rep);
            
        }

        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            

        }
    }
}
