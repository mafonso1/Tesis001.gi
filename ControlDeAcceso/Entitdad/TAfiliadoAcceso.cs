//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entitdad
{
    using System;
    using System.Collections.Generic;
    
    public partial class TAfiliadoAcceso
    {
        public int Usuarios_IdUsuarios { get; set; }
        public Nullable<System.DateTime> FechaInicio { get; set; }
        public Nullable<System.DateTime> FechaFin { get; set; }
        public Nullable<int> Puertas { get; set; }
    
        public virtual TUsuarios TUsuarios { get; set; }
    }
}
