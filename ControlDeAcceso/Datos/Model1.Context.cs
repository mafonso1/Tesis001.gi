﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Datos
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class EntityControlDeacceso : DbContext
    {
        public EntityControlDeacceso()
            : base("name=EntityControlDeacceso")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<TAfiliadoAcceso> TAfiliadoAcceso { get; set; }
        public virtual DbSet<TAfiliadoAsistencia> TAfiliadoAsistencia { get; set; }
        public virtual DbSet<TApertura> TApertura { get; set; }
        public virtual DbSet<TAuditoria> TAuditoria { get; set; }
        public virtual DbSet<TEmpleadoAsistencia> TEmpleadoAsistencia { get; set; }
        public virtual DbSet<TEmpleados> TEmpleados { get; set; }
        public virtual DbSet<THistorio> THistorio { get; set; }
        public virtual DbSet<TUsuarios> TUsuarios { get; set; }
    }
}
