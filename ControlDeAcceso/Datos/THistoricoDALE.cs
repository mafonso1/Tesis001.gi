﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entitdad;

namespace Datos
{
    public class THistoricoDALE
    {
        public List<TEmpleados> GetHistorico()
        {
            List<TEmpleados> historico = new List<TEmpleados>();
            using (EntityControlDeacceso db = new EntityControlDeacceso())
            {
                historico.AddRange(db.TEmpleados.Where(p => p.Cedula == 18935096).ToList());
                return historico;

            }
        }

    }
}
