﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;



namespace ProjectoDeGrado
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            qrCodeImgControl1.Text = textBox1.Text;
        }

        private void Agregar_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(@"Data Source=MINIONS-PC\MICHAELSERVER;Initial Catalog=Proyecto;Integrated Security=true");
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                // Estableciento propiedades 
                cmd.Connection = conn; cmd.CommandText = "INSERT INTO Table_1 VALUES (@Codigo)";
                // Creando los parámetros necesarios
                cmd.Parameters.Add("@Codigo", System.Data.SqlDbType.VarChar);
                // Asignando los valores a los atributos 
                cmd.Parameters["@Codigo"].Value = textBox1.Text; conn.Open();
                cmd.ExecuteNonQuery(); conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            MessageBox.Show("Registro Guardado Correctamente");
            textBox1.Clear();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            qrCodeImgControl1.Text = listBox1.Text;
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'proyectoDataSet.Table_1' Puede moverla o quitarla según sea necesario.
            this.table_1TableAdapter.Fill(this.proyectoDataSet.Table_1);
            qrCodeImgControl1.Text = listBox1.Text;

        

        }
    }
}

        
    

