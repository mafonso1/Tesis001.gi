﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace Datos
{
     public class UsuariosDATOS
    {

            public List<TUsuarios> ObtenerUsuarios()
            {
                List<TUsuarios> usu = new List<TUsuarios>();
                using (DBControlDeAccesoEntities db = new DBControlDeAccesoEntities())
                {
                    usu.AddRange(db.TUsuarios.Where(p => p.Cedula == 18935096).ToList());
                    return usu;

                }
            }
        }
}
