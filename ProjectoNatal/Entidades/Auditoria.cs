﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Auditoria
    {
        public Nullable<int> Empleados_IdEmpleados { get; set; }
        public int IdAuditoria { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public Nullable<System.TimeSpan> Hora { get; set; }
        public string Campo { get; set; }
        public string Valor { get; set; }
    }
}
