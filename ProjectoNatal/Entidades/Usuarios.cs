﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Usuarios
    {
        public int IdUsuarios { get; set; }
        public string Nombre { get; set; }
        public int Cedula { get; set; }
        public Nullable<System.DateTime> FechaNacimiento { get; set; }
        public string Correo { get; set; }
        //public byte[] UsuarioFoto { get; set; }
        //public byte[] UsuarioQR { get; set; }


    }
}
