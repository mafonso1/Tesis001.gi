﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Empleados
    {
        public int IdEmpleados { get; set; }
        public string Nombre { get; set; }
        public int Cedula { get; set; }
        public string EmpleadoUser { get; set; }
        public string EmpleadoPass { get; set; }
        public byte[] QREmpleado { get; set; }
    }
}
