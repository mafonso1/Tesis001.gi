﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Apertura
    {
        public int Empleados_IdEmpleados { get; set; }
        public int IdAperturas { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public Nullable<System.TimeSpan> Hora { get; set; }
        public string Motivo { get; set; }

        
    }
}
