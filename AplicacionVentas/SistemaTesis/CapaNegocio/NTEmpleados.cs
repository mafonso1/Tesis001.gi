﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;

namespace CapaNegocio
{
   public  class NTEmpleados
    {
        public static DataTable Mostrar(int parRegistrosPorPagina, int parNumeroPagina)
        {
            return new DTEmpleados().Mostrar(parRegistrosPorPagina, parNumeroPagina);
        }
    }
}
