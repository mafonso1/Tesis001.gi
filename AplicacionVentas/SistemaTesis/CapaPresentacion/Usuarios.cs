﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class Usuarios : UserControl
    {
        public Usuarios()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            Mostrar();
        }
        public void Mostrar()
        {
            this.dataGridView1.DataSource = NTEmpleados.Mostrar(20, 1);
        }
    }
}
