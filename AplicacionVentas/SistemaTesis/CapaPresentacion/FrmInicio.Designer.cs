﻿namespace CapaPresentacion
{
    partial class FrmInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button3Configuracion = new System.Windows.Forms.Button();
            this.button2Resportes = new System.Windows.Forms.Button();
            this.button1Registro = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button4Accesos = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 117);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(824, 499);
            this.panel2.TabIndex = 2;
      
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 297F));
            this.tableLayoutPanel2.Controls.Add(this.button3Configuracion, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.button2Resportes, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.button1Registro, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.button4Accesos, 3, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 13);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(658, 98);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // button3Configuracion
            // 
            this.button3Configuracion.Location = new System.Drawing.Point(183, 3);
            this.button3Configuracion.Name = "button3Configuracion";
            this.button3Configuracion.Size = new System.Drawing.Size(97, 92);
            this.button3Configuracion.TabIndex = 4;
            this.button3Configuracion.Text = "Configuracion";
            this.button3Configuracion.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3Configuracion.UseVisualStyleBackColor = true;
            // 
            // button2Resportes
            // 
            this.button2Resportes.BackgroundImage = global::CapaPresentacion.Properties.Resources.bloggif_57cede5a8f076;
            this.button2Resportes.Location = new System.Drawing.Point(93, 3);
            this.button2Resportes.Name = "button2Resportes";
            this.button2Resportes.Size = new System.Drawing.Size(84, 92);
            this.button2Resportes.TabIndex = 3;
            this.button2Resportes.Text = "Reportes";
            this.button2Resportes.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2Resportes.UseVisualStyleBackColor = true;
            this.button2Resportes.Click += new System.EventHandler(this.button2Resportes_Click);
            // 
            // button1Registro
            // 
            this.button1Registro.BackgroundImage = global::CapaPresentacion.Properties.Resources.bloggif_57cedd812f7ae;
            this.button1Registro.Location = new System.Drawing.Point(3, 3);
            this.button1Registro.Name = "button1Registro";
            this.button1Registro.Size = new System.Drawing.Size(84, 92);
            this.button1Registro.TabIndex = 2;
            this.button1Registro.Text = "Registro";
            this.button1Registro.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1Registro.UseVisualStyleBackColor = true;
            this.button1Registro.Click += new System.EventHandler(this.button1Registro_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::CapaPresentacion.Properties.Resources.descarga;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(364, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(291, 92);
            this.panel1.TabIndex = 6;
            // 
            // button4Accesos
            // 
            this.button4Accesos.Location = new System.Drawing.Point(287, 3);
            this.button4Accesos.Name = "button4Accesos";
            this.button4Accesos.Size = new System.Drawing.Size(71, 92);
            this.button4Accesos.TabIndex = 5;
            this.button4Accesos.Text = "Accesos";
            this.button4Accesos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button4Accesos.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.302325F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.69768F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 504F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(830, 619);
            this.tableLayoutPanel1.TabIndex = 0;
          
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(824, 499);
            this.panel3.TabIndex = 0;
          
            // 
            // FrmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 629);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmInicio";
            this.Text = "Control De acceso USM";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
    
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button button3Configuracion;
        private System.Windows.Forms.Button button2Resportes;
        private System.Windows.Forms.Button button4Accesos;
        private System.Windows.Forms.Button button1Registro;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
    }
}