﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class DTEmpleados
    {
        public int IdEmpleados { get; set; }
        public string Nombre { get; set; }
        public int Cedula { get; set; }
        public string EmpleadoUser { get; set; }
        public string EmpleadoPass { get; set; }
        public byte[] QREmpleado { get; set; }


        public DTEmpleados()
        {

        }
        public DTEmpleados(int parIdEmpleados,string parNombre,int parCedula,string parEmpleadoUser, string parEmpleadoPass, byte parQREmpleado)
        {
            this.IdEmpleados = parIdEmpleados;
            this.Nombre = parNombre;
            this.Cedula = parCedula;
            this.EmpleadoUser = parEmpleadoUser;
            this.EmpleadoPass = parEmpleadoPass;
            /*this.QREmpleado = parQREmpleado;*/
      
        }
        public DataTable Mostrar(int parRegistrosPorPagina, int parNumeroPagina)
        {
            DataTable TablaDatos = new DataTable("ListaDeEmpleados");
            SqlConnection con = new SqlConnection();
            try
            {
                con.ConnectionString = DConexion.cnDBControlDeAcceso;
                con.Open();

                SqlCommand query = new SqlCommand();
                query.Connection = con;

                query.CommandText = "VBusquedaEmpleados";
                query.CommandType = CommandType.StoredProcedure;

                SqlParameter registrosporpagina = new SqlParameter();
                registrosporpagina.ParameterName = "@RegistrosPorPaginas";
                registrosporpagina.SqlDbType = SqlDbType.Int;
                registrosporpagina.Value = parRegistrosPorPagina;

                query.Parameters.Add(parRegistrosPorPagina);
                /*-------------------------------------------------------*/
                SqlParameter numeropagina = new SqlParameter();
                numeropagina.ParameterName = "@NumeroPagina";
                numeropagina.SqlDbType = SqlDbType.Int;
                numeropagina.Value = parNumeroPagina;

                query.Parameters.Add(parNumeroPagina);

                query.ExecuteNonQuery();
                SqlDataAdapter AdaptadorDatos = new SqlDataAdapter(query);
                AdaptadorDatos.Fill(TablaDatos);

            }
            catch (Exception ex)
            {
                TablaDatos = null;
                throw new  Exception ("error al ejecutar el procedimiento almacenado VBusquedaEmpleados" + ex.Message);
            }

            finally
            {
                con.Close();
                
            }
            return TablaDatos;
        }
    }
}
